# SMBX2 Documentation

Welcome to the SMBX2 documentation page.

[Get Started](/general/introduction.md)

[Download SMBX2](/general/installation.md)

[The Editor](/general/editor.md)

Use the search field or the list on the left to find the documentation you are looking for.

The documentation is actively developed by the SMBX2 development team and contributors like you! If you are noticing something wrong or missing, feel free to help out.

[Contributing to the docs](/guides/contributing-to-the-docs.md)

[Random page]({RANDOM})

## Frequently viewed pages

- New to LunaLua?
 - Scroll down on the left to the "Types" section for an explanation of each variable type 
 - Scroll down on the left to the "Guides" section for various beginner guides to get you started
- TXT Config pages
 - [Block Config](/features/block-config.md) - For creating block-n.txt files
 - [BGO Config](/features/bgo-config.md) - For creating background-n.txt files
 - [Background Config](/features/parallaxing-backgrounds.md) - For creating background2-n.txt files
 - [NPC Config](/features/npc-config.md) - For creating npc-n.txt files
 - [Effect Config](/features/effect-config.md) - For creating effect-n.txt files
- Commonly used features
 - [Globals](/reference/globals.md) - Variables and functions without a class
 - [Lunalua Events](/reference/lunalua-events.md) - Functions you can put into your code with pre-defined behaviour
 - [Misc](/reference/misc.md) - Miscellaneous functions for various uses
 - [Graphics](/reference/graphics.md) - Various ways to draw images to the screen
 - [Global Memory](/reference/global-memory.md) - Memory addresses that do not belong to specific objects
 - [RNG](/reference/rng.md) - For creating randomness in your code
 - [Routine](/reference/routine.md) - For creating sequences that execute over multiple frames, like cutscenes
 - [Vector](/reference/vector.md) - Utility for a variety of math applications, like measuring distance or calculating rotated positions
 - [Lunatime](/reference/lunatime.md) - Utility for getting the time since the level started
- Object types
 - [Block](/reference/block.md) - Script reference for blocks
 - [Player](/reference/player.md) - Script reference for players
 - [NPC](/reference/npc.md) - Script reference for NPCs
- Cheat sheets
 - [Render Priority](/concepts/render-priority.md) - The priorities various things get rendered to
 - [SFX List](/concepts/sfx-list.md) - A list of all built-in sound effects
 - [Easing](/modules/easing.md) - Easing functions that can be used for making smooth motion