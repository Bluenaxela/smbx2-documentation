# Level Settings

Level settings are settings that can be selected in the editor by right-clicking in the scene and selecting "Level settings".

## Appear in Mario Challenge

If deselected, the level cannot appear in the Mario Challenge game mode.

## Level Timer

Sets a timer for a set number of seconds. When elapsed, either calls an event or kills all players. The timer can be refreshed [through lua](/reference/timer.md) or by eating green berries with Yoshi.