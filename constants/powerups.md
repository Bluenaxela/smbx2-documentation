# Powerup constants

Constants for the player powerups.

| Constant | Value | Description |
| --- | --- | --- |
| PLAYER_SMALL | 1 | No powerup/small (Tier 1). |
| PLAYER_BIG | 2 | Mushroom powerup/big (Tier 2). |
| PLAYER_FIREFLOWER | 3 | Fire Flower powerup (Tier 3). |
| PLAYER_LEAF | 4 | Leaf powerup (Tier 3). |
| PLAYER_TANOOKI | 5 | Tanooki Suit powerup (Tier 3). |
| PLAYER_HAMMER | 6 | Hammer Suit powerup (Tier 3). |
| PLAYER_ICE | 7 | Ice Flower powerup (Tier 3). |

Aliases:

| Constant | Value | Description |
| --- | --- | --- |
| POWERUP_SMALL | 1 | No powerup/small (Tier 1). |
| POWERUP_BIG | 2 | Mushroom powerup/big (Tier 2). |
| POWERUP_FIREFLOWER | 3 | Fire Flower powerup (Tier 3). |
| POWERUP_LEAF | 4 | Leaf powerup (Tier 3). |
| POWERUP_TANOOKI | 5 | Tanooki Suit powerup (Tier 3). |
| POWERUP_HAMMER | 6 | Hammer Suit powerup (Tier 3). |
| POWERUP_ICE | 7 | Ice Flower powerup (Tier 3). |