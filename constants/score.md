# Score constants

These constants represent the values of the score effect used by Misc.givePoints().

| Constant | Value | Description |
| --- | --- | --- |
| SCORE_0 | 0 | 0 points. |
| SCORE_10 | 1 | 10 points. |
| SCORE_100 | 2 | 100 points. |
| SCORE_200 | 3 | 200 points. |
| SCORE_400 | 4 | 400 points. |
| SCORE_800 | 5 | 800 points. |
| SCORE_1000 | 6 | 1000 points. |
| SCORE_2000 | 7 | 2000 points. |
| SCORE_4000 | 8 | 4000 points. |
| SCORE_8000 | 9 | 8000 points. |
| SCORE_1UP | 10 | 1UP. |
| SCORE_2UP | 11 | 2UP. |
| SCORE_3UP | 12 | 3UP. |
| SCORE_5UP | 13 | 5UP. |