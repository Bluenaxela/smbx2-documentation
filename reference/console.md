# Console

The console namespace lets you print text to the console. The console is enabled through [command line arguments](/features/command-line.md) when starting the game. The default instance of the console window is called "console".

## Static methods

[What is a method?](/types/function.md#methods)

{STARTTABLE}
   {NAME} Function
    {RET} Return Values
   {DESC} Description
====
   {NAME} Console:print(

[string](/types/string.md) text

)
    {RET} [nil](/types/nil.md)
   {DESC} Prints the text to the console window.
====
   {NAME} Console:println(

[string](/types/string.md) text

)
    {RET} [nil](/types/nil.md)
   {DESC} Prints the text to the console window and adds a linebreak (\n).
{ENDTABLE}